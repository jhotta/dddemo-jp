package main

import (
	"log"
	"strconv"
	"github.com/garyburd/redigo/redis"
	"io"
	"bitbucket.org/ddevangelist/dddemo/lib"
	"net/http"
	"strings"
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
)

var (
	appPort        = ":8000"
)

func increment(w http.ResponseWriter, r *http.Request) {
	// importtweets()
	// io.WriteString(w, strconv.FormatInt(counter.Rate(), 10))
	io.WriteString(w, "theres")
}

func main() {
	m := martini.Classic()

	redisPool := redis.NewPool( dd.NewConn, 10)
	defer redisPool.Close()

	m.Map(redisPool)
	m.Use(render.Renderer())
	m.Use(martini.Static("assets", martini.StaticOptions{Fallback: "/assets"}))

	m.Get("/", mainPage)
	m.Get("/resetredis", resetRedis)

	m.RunOnAddr(appPort)
}

// func getTweetRange(start int, end int, red redis.Conn) []Tweet {
// 	tweets, err := redis.Values(red.Do("ZRANGE", tweetIdsKey, start, end))
// 	dd.PanicIf("Problem with redis ZRANGE", err)
// 	for _, tweetid := range tweets {
// 		tweet, err := redis.String(red.Do("GET", tweetid))
// 		dd.PanicIf("Error reading a tweet", err)
// 		if err := redis.ScanSlice(src, dest, ...)
// 		output += tweet + "\r\n"
// 	}
// 	pagedTweets, _ := red.Do("MGET", tweets)

// }

func mainPage(r render.Render, pool *redis.Pool,req *http.Request)  {
	// output := ""

	quantity:=20
	red := pool.Get()
	defer red.Close()
	dd.PopulateRedis(red)
	params := req.URL.Query()

	page, _ := strconv.Atoi(params.Get("page"))
	// log.Print(m["page"])
	start:=page * quantity
	end:=start + quantity
	log.Print(start)
	tweets:=dd.GetTweets(start, end, red)

	pageData:= PageData{}
	pageData.NextPage=strconv.Itoa(page+1)
	pageData.PrevPage=strconv.Itoa(page-1)
	pageData.Tweets=tweets

	r.HTML(200, "main", pageData)
}



type PageData struct {
	Tweets []dd.Tweet
	NextPage string
	PrevPage string
}
func resetRedis(pool *redis.Pool) string {
	red := pool.Get()
	defer red.Close()
	dd.ResetRedis(red)
	return "Redis Reset"
}

func wordcount(tweet string) map[string]int {
	substrs := strings.Fields(tweet)

	counts := make(map[string]int)

	for _, word := range substrs {
		_, ok := counts[word]

		if ok {
			counts[word] += 1
		} else {
			counts[word] = 1
		}
	}
	return counts
}

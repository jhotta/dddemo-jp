package dd

import (
	"strings"
	"time"
	"os"
	"bytes"
	"encoding/csv"
	"github.com/mitchellh/goamz/s3"
	"github.com/mitchellh/goamz/aws"
	"log"
	"github.com/garyburd/redigo/redis"
)

var (
  awsAuth        aws.Auth
  s3conn         *s3.S3
  s3bucket       = "evangelism"
  tweetIdsKey    = "tweetids"
  redisipaddress = "192.168.59.103:6379"

)
const (
  tweetTag = "tweet:"
)


type Tweet struct {
  TweetId           string `redis:"TweetId"`
  InReplyToStatusId string `redis:"InReplyToStatusId"`
  InReplyToUserId   string `redis:"InReplyToUserId"`
  Timestamp         string `redis:"Timestamp"`
  Source            string `redis:"Source"`
  Text              string `redis:"Text"`
  RetweetedStatusId string `redis:"RetweetedStatusId"`
  RetweetedStatusUserId string `redis:"RetweetedStatusUserId"`
  RetweetedStatusTimestamp string `redis:"RetweetedStatusTimestamp"`
  ExpandedUrls string `redis:"ExpandedUrls"`
}

func NewConn() (redis.Conn, error) {
  log.Print("Trying local Redis")
  c, err := redis.Dial("tcp", "redis:6379")
  if err != nil {
    log.Print("Trying other Redis")
    c, err = redis.Dial("tcp", redisipaddress)
  }
  PanicIf("Redis Connection Error", err)
  return c, err
}

func ResetRedis(red redis.Conn) {
  log.Print("In Reset Redis")
  getTweetsFromCSV(red)
}

func getTweetsFromCSV(red redis.Conn) {
  log.Print("Populating Redis")

  tweetfile, err := os.Open("tweets.csv")
  var reader *csv.Reader
  if os.IsNotExist(err) {
    log.Print("Use AWS")

    awsAuth, err := aws.EnvAuth()
    PanicIf("Problem with AWS Auth", err)
    s3conn = s3.New(awsAuth, aws.USWest2)
    bucket := s3conn.Bucket(s3bucket)
    tweetbytes, err := bucket.Get("tweets.csv")
    PanicIf("problem getting csv from s3", err)
    reader = csv.NewReader(bytes.NewReader(tweetbytes))
  } else {
    reader = csv.NewReader(tweetfile)
  }


  reader.Read()
  reader.FieldsPerRecord = -1
  rawCSVdata, err := reader.ReadAll()
  PanicIf("Problem reading CSV", err)
  for index, each := range rawCSVdata {
    tweet:=csvToTweet(each)
    _, err := red.Do("HMSET", tweetTag+tweet.TweetId,
      "TweetId", tweet.TweetId,
      "InReplyToStatusId", tweet.InReplyToStatusId,
      "InReplyToUserId", tweet.InReplyToUserId,
      "Timestamp", tweet.Timestamp,
      "Source", tweet.Source,
      "Text", tweet.Text,
      "RetweetedStatusId", tweet.RetweetedStatusId,
      "RetweetedStatusUserId", tweet.RetweetedStatusUserId,
      "RetweetedStatusTimestamp", tweet.RetweetedStatusTimestamp,
      "ExpandedUrls", tweet.ExpandedUrls)
    PanicIf("Problem with redis set", err)
    if index%1000 == 0 {
      log.Printf("%d tweets added", index)
    }
    red.Do("ZADD", tweetIdsKey, index, each[0])
    // log.Print(each)
    // tweetstr := each[0] + ":" + each[5]
    // tweetstr, _ := redis.String(red.Do("GET", each[0]))
    //  // currenttweet.Set(tweetstr)
    // for word, count := range wordcount(tweetstr) {
    //  savedcount, err := redis.Int(red.Do("GET", word))
    //  if err != nil {
    //    savedcount = 0
    //  }
    //  red.Do("SET", word, count+savedcount)
    // }
    // time.Sleep(100 * time.Millisecond)
  }
  log.Print("Redis Populated")
}

func csvToTweet(line []string) *Tweet {
  tweet := new(Tweet)
  tweet.TweetId=line[0]
  tweet.InReplyToStatusId=line[1]
  tweet.InReplyToUserId=line[2]
  t, _ := time.Parse("2006-01-02 15:04:05 -0700", line[3])
  tweet.Timestamp= t.Format("01/02/06")
  first:= strings.Index(line[4], ">")+1
  last := strings.LastIndex(line[4], "<")

  tweet.Source=line[4][first:last]
  tweet.Text=line[5]
  tweet.RetweetedStatusId=line[6]
  tweet.RetweetedStatusUserId=line[7]
  tweet.RetweetedStatusTimestamp=line[8]
  tweet.ExpandedUrls=line[9]
  return tweet
}
func GetTweets(start int, end int, red redis.Conn) []Tweet {

  // tweets, err := redis.ScanStruct(red.Do("ZRANGE", tweetIdsKey, start, end))
  tweetIds, err :=redis.Strings(red.Do("ZREVRANGE", tweetIdsKey, start, end))
  PanicIf("Problem getting range of tweets", err)

  tweets := []Tweet{}

  for _, tweetid := range tweetIds{
    val, err := redis.Values(red.Do("HGETALL", tweetTag+tweetid))
    PanicIf("Problem getting "+tweetTag+tweetid, err)

    t:=new(Tweet)
    err = redis.ScanStruct(val, t)
    PanicIf("Proble with ScanStruct in GetTweets", err)
    tweets=append(tweets, *t)
  }
  return tweets
}
func PopulateRedis(red redis.Conn) {
  log.Print("In Populate Redis")
  if redisNeedsTweets(red) {
    getTweetsFromCSV(red)
  }
}

  //     file, err := os.OpenFile("tweets.csv")
  // if os.IsNotExist(err {
  //   useAWS := true
  // }
func redisNeedsTweets(red redis.Conn) bool {
  output := true
  allTweets, err := redis.Strings(red.Do("KEYS", "tweet:*"))
  PanicIf("problem getting DBSIZE", err)
  log.Printf("Number of Tweets in Redis: %d", len(allTweets))
  if len(allTweets) > 10 {
    output = false
  }
  return output
}

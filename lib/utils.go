package dd

import (
	"fmt"
	"log"
)

func PanicIf(preErr string, err error) {
  if err != nil {
    log.Panicf("%s: %s", preErr, err.Error())
  }
}

func Test( )  {
  fmt.Println("blah")
}
